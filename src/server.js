const Koa = require('koa')
const passport = require('koa-passport')
const bodyparser = require('koa-bodyparser')
const session = require('koa-session')

const createRouter = require('./router')

const isDev = process.env.NODE_ENV !== 'production'
const isProd = process.env.NODE_ENV === 'production'

module.exports = async function createApp() {
  const app = new Koa()
  app.use(bodyparser())

  // Add some assertions required in a production environment
  if (isProd) {
    assert(process.env.SECRET_KEY, 'Please set SECRET_KEY env variable.')
  }
  app.keys = [process.env.SECRET_KEY || 'SECRET_KEY']
  app.use(session(app))
  app.use(passport.initialize())
  app.use(passport.session())

  const router = await createRouter()
  app.use(router.routes())

  return app
}
