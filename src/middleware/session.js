const session = require('koa-session')
const RedisStore = require('koa-redis')
const ONE_DAY = 1000 * 60 * 60 * 24

const isDev = process.env.NODE_ENV !== 'production'
const isProd = process.env.NODE_ENV === 'production'
const store = new RedisStore()

export default function installSession(app) {
  // Add some assertions required in a production environment
  if (isProd) {
    assert(process.env.SECRET_KEY, 'Please set SECRET_KEY env variable.')
  }
  app.keys = [process.env.SECRET_KEY || 'SECRET_KEY']

  app.use(session({
      store: new RedisStore({
        host: 'localhost',
        port: 6379
      })
  }))
}
